%define debug_package %{nil}

Name:           go-containerregistry
Version:        0.20.2
Release:        1%{?dist}
Summary:        Go library and CLIs for working with container registries.

License:        ASL 2.0
URL:            https://github.com/google/go-containerregistry
Source0:        https://github.com/google/%{name}/releases/download/v%{version}/%{name}_Linux_x86_64.tar.gz

%description
Go library and CLIs for working with container registries.

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 crane %{buildroot}/usr/bin
install -p -m 755 gcrane %{buildroot}/usr/bin
install -p -m 755 krane %{buildroot}/usr/bin

%files
%license LICENSE
%doc README.md
/usr/bin/crane
/usr/bin/gcrane
/usr/bin/krane

%changelog
* Mon Dec 09 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Thu Apr 11 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 
* Sun Mar 03 2024 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to v0.19.0

* Thu Dec 28 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.17.0

* Thu Aug 31 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Bumped to version 0.16.1

* Sun May 21 2023 Enmanuel Moreira <enmanuelmoreira@gmail.com> - 0.15.2-1
- Bumped to v0.15.2
